package Isof.tarea;

import Isof.tarea.service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TareaApplicationTests {

    @BeforeEach
    void setup() {

        String apellidoPaterno = "Perez";
        String apellidoMaterno = "Gonzalez";
        String rut = "123456789";
        String telefono = "12345678";
        String edad = "20";
    }

    @Test
    @DisplayName("Validar nombre")
    void nombreValido() {
        String nombre = "Juan";
        Assertions.assertTrue(UsuarioService.validarNombre(nombre));

    }

    @Test
    @DisplayName("Validar apellido paterno")
    void apellidoPaternoValido() {
        String apellidoPaterno = "Perez";
        Assertions.assertTrue(UsuarioService.validarApellidoPaterno(apellidoPaterno));

    }

    @Test
    @DisplayName("Validar apellido materno")
    void apellidoMaternoValido() {
        String apellidoMaterno = "Gonzalez";
        Assertions.assertTrue(UsuarioService.validarApellidoMaterno(apellidoMaterno));

    }

    @Test
    @DisplayName("Validar rut")
    void rutValido() {
        String rut = "123456789";
        Assertions.assertTrue(UsuarioService.validarRut(rut));

    }

    @Test
    @DisplayName("Validar telefono")
    void telefonoValido() {
        String telefono = "123456789";
        Assertions.assertTrue(UsuarioService.validarTelefono(telefono));

    }

    @Test
    @DisplayName("Validar edad")
    void edadValida() {
        String edad = "20";
        Assertions.assertTrue(UsuarioService.validarEdad(edad));

    }

    //ahora todos los test anteriores pero en version erronea
    @Test
    @DisplayName("Validar nombre erroneo")
    void nombreErroneo() {
        String nombre = "J";
        Assertions.assertFalse(UsuarioService.validarNombre(nombre));

    }

    @Test
    @DisplayName("Validar apellido paterno erroneo")
    void apellidoPaternoErroneo() {
        String apellidoPaterno = "P";
        Assertions.assertFalse(UsuarioService.validarApellidoPaterno(apellidoPaterno));

    }

    @Test
    @DisplayName("Validar apellido materno erroneo")
    void apellidoMaternoErroneo() {
        String apellidoMaterno = "G";
        Assertions.assertFalse(UsuarioService.validarApellidoMaterno(apellidoMaterno));

    }

    @Test
    @DisplayName("Validar rut erroneo")
    void rutErroneo() {
        String rut = "12345";
        Assertions.assertFalse(UsuarioService.validarRut(rut));

    }

    @Test
    @DisplayName("Validar telefono erroneo")
    void telefonoErroneo() {
        String telefono = "1234567";
        Assertions.assertFalse(UsuarioService.validarTelefono(telefono));

    }

    @Test
    @DisplayName("Validar edad erronea")
    void edadErronea() {
        String edad = "0";
        Assertions.assertFalse(UsuarioService.validarEdad(edad));

    }


}
