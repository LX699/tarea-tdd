package Isof.tarea.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Usuario {
    // Atributos nombre, apellido paterno, apellido materno, rut, telefono, edad
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String rut;
    private int telefono;
    private int edad;

    // Constructor con todos los atributos
    public Usuario(String nombre, String apellidoPaterno, String apellidoMaterno, String rut, int telefono, int edad) {
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.rut = rut;
        this.telefono = telefono;
        this.edad = edad;
    }

}