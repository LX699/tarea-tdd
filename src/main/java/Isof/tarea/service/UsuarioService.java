package Isof.tarea.service;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UsuarioService {

    public static boolean validarNombre(String nombre) {
        // Validar que el nombre tenga más de 2 caracteres y menos de 30
        if (nombre.length() <= 2 || nombre.length() > 30) {
            return false;
        }

        // Validar que el nombre no contenga números
        if (Pattern.matches(".*\\d+.*", nombre)) {
            return false;
        }

        return true;
    }

    public static boolean validarApellidoPaterno(String apellidoPaterno) {
        // Validar que el apellido paterno tenga más de 3 caracteres y no supere los 20 caracteres
        if (apellidoPaterno.length() <= 3 || apellidoPaterno.length() > 20) {
            return false;
        }

        // Validar que el apellido paterno no contenga números
        if (Pattern.matches(".*\\d+.*", apellidoPaterno)) {
            return false;
        }

        return true;
    }

    public static boolean validarApellidoMaterno(String apellidoMaterno) {
        // Validar que el apellido materno tenga más de 3 caracteres y no supere los 20 caracteres
        if (apellidoMaterno.length() <= 3 || apellidoMaterno.length() > 20) {
            return false;
        }

        // Validar que el apellido materno no contenga números
        if (Pattern.matches(".*\\d+.*", apellidoMaterno)) {
            return false;
        }

        return true;
    }

    public static boolean validarRut(String rut) {
        // Validar que el RUT tenga más de 7 caracteres
        if (rut.length() < 7) {
            return false;
        }

        // Validar que todos los caracteres, excepto el último, sean dígitos numéricos
        for (int i = 0; i < rut.length() - 1; i++) {
            if (!Character.isDigit(rut.charAt(i))) {
                return false;
            }
        }

        // Validar que el último carácter sea un dígito numérico o una 'k' (representando el dígito verificador)
        char ultimoCaracter = rut.charAt(rut.length() - 1);
        if (!Character.isDigit(ultimoCaracter) && ultimoCaracter != 'k' && ultimoCaracter != 'K') {
            return false;
        }

        return true;
    }

    public static boolean validarTelefono(String telefono) {
        // Validar que el teléfono tenga 9 caracteres
        if (telefono.length() != 9) {
            return false;
        }

        // Validar que todos los caracteres sean dígitos numéricos
        for (int i = 0; i < telefono.length(); i++) {
            if (!Character.isDigit(telefono.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    public static boolean validarEdad(String edad) {
        // Validar que la edad tenga 1 a 3 caracteres
        if (edad.length() < 1 || edad.length() > 3) {
            return false;
        }

        // Validar que todos los caracteres sean dígitos numéricos
        for (int i = 0; i < edad.length(); i++) {
            if (!Character.isDigit(edad.charAt(i))) {
                return false;
            }
        }

        return true;
    }


}
