package Isof.tarea.controller;

import Isof.tarea.model.Usuario;
import Isof.tarea.service.UsuarioService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UsuarioController {

    @PostMapping("/crear-usuario")
    public ResponseEntity<String> crearUsuario(@RequestParam String nombre,
                                               @RequestParam String apellidoPaterno,
                                               @RequestParam String apellidoMaterno,
                                               @RequestParam String rut,
                                               @RequestParam String telefono,
                                               @RequestParam String edad) {

        UsuarioService usuarioService = new UsuarioService();
        usuarioService.validarNombre(nombre);
        usuarioService.validarApellidoPaterno(apellidoPaterno);
        usuarioService.validarApellidoMaterno(apellidoMaterno);
        usuarioService.validarRut(rut);
        usuarioService.validarTelefono(telefono);
        usuarioService.validarEdad(edad);

        String resultado = "";
        resultado += "Nombre Correcto = " + usuarioService.validarNombre(nombre) + "\n";
        resultado += "Apellido Paterno Correcto = " + usuarioService.validarApellidoPaterno(apellidoPaterno) + "\n";
        resultado += "Apellido Materno Correcto = " + usuarioService.validarApellidoMaterno(apellidoMaterno) + "\n";
        resultado += "Rut Correcto = " + usuarioService.validarRut(rut) + "\n";
        resultado += "Telefono Correcto = " + usuarioService.validarTelefono(telefono) + "\n";
        resultado += "Edad Correcta = " + usuarioService.validarEdad(edad);


        return ResponseEntity.ok(resultado);
    }
}
